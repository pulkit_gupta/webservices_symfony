<?php

// src/AppBundle/Controller/LuckyController.php

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Config\Definition\Exception\Exception;

use AppBundle\Entity\Product;

class RestController extends Controller
{
    /**
     * @Route("/lucky/number")
     */
    public function numberAction()
    {
        $number = rand(0, 100);

        return new Response(
            '<html><body>Lucky number: '.$number.'</body></html>'
        );
    }

    /**
    *   @Route("/api/post")
    *
    */
    public function yourAction()
    {
        $params = array();
        $content = $this->get("request")->getContent();
        if (!empty($content))
        {
            $params = json_decode($content, true); // 2nd param to get as array
        }
        else{
            //throw $this->createNotFoundException();

            return new Response("",404);
        }
       return new Response(
                $content,
                201,
                array('Content-Type' => 'application/json')
            );
    }

    /**
         * @Route("/api/lucky/number")
    */
        public function apiNumberAction()
        {
            $data = array(
                'lucky_number' => rand(0, 100),
            );

            return new JsonResponse($data);
                
        }

        /**
        *   @Route("/api/post/product/{name}")
        *
        */

        public function createProduct($name)
        {
            $content = $this->get("request")->getContent();
                
            $json = json_decode($content, true);

            //echo "$content";
            $product = new Product();
            $product->setName($name);
            $product->setPrice($json['price']);
            $product->setDescription('Lorem ipsum dolor');

            $em = $this->getDoctrine()->getManager();

            $em->persist($product);
            $em->flush();

            return new Response('Created product id '.$product->getId());
        }


    /**
     * @Route(
     *     "/articles/{_locale}/{year}/{title}.{_format}",
     *     defaults={"_format": "html"},
     *     requirements={
     *         "_locale": "en|fr",
     *         "_format": "html|rss",
     *         "year": "\d+"
     *     }
     * )
     */
    public function showAction($_locale, $year, $title)
    {
    	$data = array(
                'local' => $_locale,
                'year' => $year,
                'title' => $title
            );  
            return new Response(
                json_encode($data),
                200,
                array('Content-Type' => 'application/json')
            );
    }
}
